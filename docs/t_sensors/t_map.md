On each sTGC wedge there are 13 T sensors, 9 of them glued on top of the protection layer, plus 4 T sensors for cooling.
1 input/out for each side (rising, lowering)

The labels for the T sensors are the following:

# ELMB Mapping

In total 26 T sensors are connected to the ELMB module.
Each ELMB module (see picture) has a Node number, there are 2 ELMB modules per sector (1 for sTGC and 1 for MM). There are all located at the Large sector spokes. 

The T sensor to pin number mapping is the following:

## Small Sector

| Pin | IP - CONFIRM  | Pin |  HO - PIVOT   |
| --- | ------------- | --- | ------------- |
| 0   | IP - SC - 1   | 15  |  HO - SP-1    |
| 1   | IP - SC - 2L  | 16  |  HO - SP-2L   |
| 2   | IP - SC - 2R  | 17  |  HO - SP-2R   |
| 3   | IP - SC - 3   | 18  |  HO - SP-3    |
| 4   | IP - SC - 4L  | 19  |  HO - SP-4L   |
| 5   | IP - SC - 4R  | 20  |  HO - SP-4R   |
| 6   | IP - SC - 5   | 21  |  HO - SP-5    |
| 7   | IP - SC - 6L  | 22  |  HO - SP-6L   |
| 8   | IP - SC - 6R  | 23  |  HO - SP-6R   |
| 9   | SPARE         | 24  |  SPARE        |
| 10  | SPARE         | 25  |  SPARE        |
| 11  | IP-Cool-L-IN  | 26  | HO-Cool-L-IN  |
| 12  | IP-Cool-L-Out | 27  | HO-Cool-L-Out |
| 13  | IP-Cool-R-IN  | 28  | HO-Cool-R-IN  |
| 14  | IP-Cool-R-Out | 29  | HO-Cool-R-Out |


