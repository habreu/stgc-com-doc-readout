# NSW sTGC Commissioning documentation site

All documentation about mappings, how-to guides, etc will be publish in this site. 

Sectors under Commissioning

- **A14**
- **A12**

## Low voltage control:

| Branch Controller | Connector |  Sector | ICS Location | Date |
| :---: | :---: | :---: | :---: | :---: |
| 11 | B | **A12** | A13 | 18/02 |
| 15 | B | **A14** | A15 | 19/02 |



## RIM
| Branch Controller | Connector |  Sector | Channels | Date |
| :---: | :---: | :---: | :---: | :---: |
| 10 | A | **A10** | ch1 & ch2 | 28.01.21 |

## 300V Connector

| ICS powered | Date |
| :---:  | :---: |
| 13 | 18/02 |
| 15 | 19/02 |

## Fiber connections
| Felix Machine | Fiber Bundle |  Sector  | Configuration | Date |
| :---: | :---: | :---: | :---: | :---: |
| `felix05` | 1 | **A12** | Small Sector |  18/02   |
| `felix08` | 2 | **A14** | Small Sector | 18/02 |
| `felix08` | 2 | **A13** | Large Sector | 18/02 |
| `tp` | trigger | **A14** | Small Sector | 10.02.21 |





## High Voltage Control:

At the FSM panel there are two sectors for HV:   

| HV Bundle | FMS Sector Name |  Sector  |  Date | Comment |
| :---: | :---: | :---: | :---: | :---: |
| **S01** | 1 | **A16**  | 19.02.21 | B to 16B (A is being repaired)|
| **S02** | 2 | **A16**  | 19.02.21 | A to 16A (B is being repaired) |



## Sector situation

The test status of the different sectors can be found here:  
[https://docs.google.com/spreadsheets/d/1pjRe_sInoPYsNVWJbgmq80IvB5iv1PbESc7IBFJUM1Q/edit#gid=1877449389](https://docs.google.com/spreadsheets/d/1pjRe_sInoPYsNVWJbgmq80IvB5iv1PbESc7IBFJUM1Q/edit#gid=1877449389)




-----

## Powered by

 * [mkdocs](https://www.mkdocs.org/)
 * [mkdocs-material](https://github.com/squidfunk/mkdocs-material)

