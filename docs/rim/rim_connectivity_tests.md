# Rim crate connectivity tests

## Debug

Issues and trouble-shouting is described here:
[https://gitlab.cern.ch/gvasquez/stgc-com-doc-readout/-/blob/master/docs/rim/debuggin.md](https://gitlab.cern.ch/gvasquez/stgc-com-doc-readout/-/blob/master/docs/rim/debuggin.md)


## Instructions

!!! Warning
    Always monitor temperature and do not forget to turn off the LV when the tests are done. Do not leave LV unattended 

The latest (2020-11-05) instructions to run the pad trigger connectivity test are found here:
[https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/53](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/53)  
This section is a copy with a bit more details for the non-experts of 191.

### pFEB to PAD connectivity

#### Check config files:

* Check JSON and XML files as indicated on [https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/53](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/53)
* Make sure you are using the right sector, don't forget to adapt the commands below. They have been written for A08. 

#### Run test
Login on felix05, configure elinks and upload bitfile on pad trigger:
```
ssh -Y nswdaq@pcatlnswfelix05
elinkconfig_sTGC_191 HOIPTrigger_SS 
fscajtag -d 1 -R 10 -e 2ff /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_ro_ilaro_20200610.bit
```
Start felixcore and opc server (for pad trigger temperture monitoring)
```
felixcore --felix-id 5 --data-interface eno1 --elinks 0xae1,0x33b
#new terminal
ssh -Y nswdaq@pcatlnswfelix05
export XML="/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/opc_xml/191/A08/A08_STG_191_as_Small_Sector_FlxId5_Trigger.xml"
OpcUaScaServer $XML
```
Monitor pad trigger temperature. A temperature of 40C is expected. A temperature above 50C becomes dangerous.
```
export=JSON="/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/A08/STGC_191_A08_HOIP_TestPulse_as_Small_Sector_Trigger.json"
while true; do echo $(date) $(nsw_pad_trigger --config_file $JSON --i2c_reg 1 | grep Readback); sleep 30; done
```
If not working make sure the json is correct. If ok, you need an expert to check the temperure with `vivado`.

Now we will start the parition on software rod3 to take data:
```
stgc_connectivity
source NSWPartitionMaker/191A-TriggerConnectivity/setup_part.sh
run
```
The run GUI with appear. You can continue using the same terminal for the upcoming commands.  
On the GUI, subscribe to `information` and increase number of messages to 10 000.

* **Pad connectivity to pFEB:**  

Click `initialise` then run the following command before configuring:
```
is_write -p part-191A-TriggerConnectivity -n Setup.NSW.calibType -t String  -v sTGCPadConnectivity -i 0  
```
You can then start the run.
It does not stop automatically, you should stop the run when you see `NSWCalibRc::handler::End of handler`
```
configure
run
# when done
stop
```

* **Latency test:**  

Same as before but run instead:
```
is_write -p part-191A-TriggerConnectivity -n Setup.NSW.calibType -t String  -v sTGCPadLatency -i 0
```

#### Plotting
Plotting is done via a gitlab pipeline. You might need to request permission to Alex if running for the first time.
```
cd /tmp/nswdaq
cp YourDataFile.data /eos/atlas/atlascerngroupdisk/det-nsw/191/trigger/data/Sector_Name
GitLab CI/CD run pipeline (https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/pipelines)
```
Once on gitlab, clic on `run pipeline` and then and the variable `SECTOR` and your sector number, e.g `A08`

### PT - sFEB

See [https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/53](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/53)

### sFEB to router

See [https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/53](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/53)



---

## Trigger Processor (TP)

Full instructions available here:

[https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/61](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/61)

### Power TP and setup ALTI

1. power the TP on: [http://nsw-pdu03.cern.ch/](http://nsw-pdu03.cern.ch/)
   The ID & pwd is `admin`

2. Connect the Felix07 fiber to the sTGC ATLTI

   ![image](../images/rimcrate/Felix07Fiber.jpg)



### Follow the Gitlab instructions

This section gives brief information about what the commands mean/do and adds a couple of screenshots. You should follow the gitlab instructions (updated more often) in case anything differs.
[https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/61](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/61)

#### 1. Configure ALTI pattern

These 2 lines configure ALTI to send the right patters to the FEBs. **Do not forget to switch it off when you are done!** If not it will cause issues for other readout tests.

```bash
load_pattern /afs/cern.ch/user/n/nswdaq/public/alti/pg_alti_1pulse_perLHCorbit.dat
start_pattern
```

#### 2. elink, felix, OPC

configure elinks, start felixcore on felix05 and Run OPC server. 
**Felix05** is used to configure the FEBS and routers. 
**Felix07** will be used for the readout.

#### 3. Configure FEBs and routers

This is just to modify the default ERROR/INFO printout which is too talkative:

```
export TDAQ_ERS_INFO="throttle(100000, 10),stdout"
export TDAQ_ERS_WARNING="throttle(100000, 10),stdout"
```

Configure routers and FEBs

```
stgc_connectivity && cd x86_64-centos7-gcc8-opt/NSWConfiguration
./configure_frontend -v -r -t --resetvmm -c $JSON_ROUTERS
./nsw_router -c $JSON_ROUTERS -n ""
```

* When running `configure_frontend` it is ok to see error messages for the pad and router, such as:
  `PadTriggerSCA_00 - ERROR: Skipping this FE! - Problem constructing configuration due to : No such node (rocPllCoreAnalog)`
* on A08 `PFEB_L1Q3_IPR` cannot be accessed (commented out) and Layer 5 is bad because not connected.
* When running `nsw_router`, all routers should display all `Good` as shown here:

```
2020-Nov-18 09:56:58,558 INFO [nsw::ConfigSender::sendRouterCheckGPIO(...) at /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWConfiguration/src/ConfigSender.cpp:983] Router_A08_L7.gpio.fpgaConfigOK    :: Expected = 1 Observed = 1 -> Good
2020-Nov-18 09:56:59,560 INFO [nsw::ConfigSender::sendRouterCheckGPIO(...) at /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWConfiguration/src/ConfigSender.cpp:983] Router_A08_L7.gpio.mmcmBotLock     :: Expected = 1 Observed = 1 -> Good
2020-Nov-18 09:56:59,563 INFO [nsw::ConfigSender::sendRouterCheckGPIO(...) at /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWConfiguration/src/ConfigSender.cpp:983] Router_A08_L7.gpio.fpgaInit        :: Expected = 1 Observed = 1 -> Good
2020-Nov-18 09:56:59,565 INFO [nsw::ConfigSender::sendRouterCheckGPIO(...) at /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWConfiguration/src/ConfigSender.cpp:983] Router_A08_L7.gpio.rxClkReady      :: Expected = 1 Observed = 1 -> Good
2020-Nov-18 09:56:59,568 INFO [nsw::ConfigSender::sendRouterCheckGPIO(...) at /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWConfiguration/src/ConfigSender.cpp:983] Router_A08_L7.gpio.txClkReady      :: Expected = 1 Observed = 1 -> Good
2020-Nov-18 09:56:59,570 INFO [nsw::ConfigSender::sendRouterCheckGPIO(...) at /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWConfiguration/src/ConfigSender.cpp:983] Router_A08_L7.gpio.cpllTopLock     :: Expected = 1 Observed = 1 -> Good
2020-Nov-18 09:56:59,573 INFO [nsw::ConfigSender::sendRouterCheckGPIO(...) at /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWConfiguration/src/ConfigSender.cpp:983] Router_A08_L7.gpio.cpllBotLock     :: Expected = 1 Observed = 1 -> Good
2020-Nov-18 09:56:59,575 INFO [nsw::ConfigSender::sendRouterCheckGPIO(...) at /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWConfiguration/src/ConfigSender.cpp:983] Router_A08_L7.gpio.mmcmTopLock     :: Expected = 1 Observed = 1 -> Good
2020-Nov-18 09:56:59,577 INFO [nsw::ConfigSender::sendRouterCheckGPIO(...) at /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWConfiguration/src/ConfigSender.cpp:983] Router_A08_L7.gpio.semFatalError   :: Expected = 0 Observed = 0 -> Good
2020-Nov-18 09:56:59,580 INFO [nsw::ConfigSender::sendRouterCheckGPIO(...) at /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWConfiguration/src/ConfigSender.cpp:983] Router_A08_L7.gpio.masterChannel0  :: Expected = 1 Observed = 1 -> Good
```



#### 4. vivado

Open vivado and load the devices with the automatic setup. You should see the 3 FPGAs of the TP cards as shown below:

![](../images/rimcrate/vivado.png)

* Start by programming the sTGC FPGA (device_2), right click on it, then load/program? Then copy the bitfile address.

* Do the same for device_0

**VIO**: Virtual input/Output. 
The gbtx polarity is not defined by default and should be added by hand. Follow the instructions to set the `rxPolarity` to 1 in the VIO tab . click on the + to add a device and search for that one.

Once this is done, run `flx-info gbt` on **felix07**, you should see:

```
This is an FLX-709

GBT CHANNEL ALIGNMENT STATUS
Channel |  0    1    2    3  
        --------------------
Aligned | YES  YES  NO   NO 
```



**ILA** =  the equivalent of an internal oscilloscope. Double click on the device_2. Go to ILA4 (last tab) and delete all existing ILA. Add new probes as indicated on gitlad

**RST**: reset all the ILA by going to `RST_RX`, changing all values to 1 (1FF) and then back to zero. If you get noise or any problems, you can repeat this procedure

#### 5. enable/disable tds

**This is the actual test where we will readout the tds.**

Get the list of sFEB names with:

```
grep Node $JSON_ROUTERS | grep SFEB
```

Then you can enable/disable them by running:

```
./nsw_tds_frame2Router -m test_frame2Router_enable --enable -c $JSON_ROUTERS
 -s SFEB_L1Q1_HOL -t tds0
```

Run for all sFEB and tds0 to tds3. Note that sFEB6 (for Q2 and Q3) do not have a tds0.

For each tds:

* run the enable command
* on vivado, click on `run trigger` (triangle sign for reading). You should see a signal on the`valid_data` ILA. note the number (0 to 31)
* run the disable command
* on vivado, click on `run trigger` (triangle sign for reading). The signal should be gone.

When 1 tds is enabled on one FEB, the same link displays the signal no matter which tds in enabled. If all 3/4 tds are enabled on a given FEB, then several links are used and should display data



---

## OLD Instructions - Pad trigger connectivity

**These instructions ar ethe old version, kept here in case we need them again. But please you the section above unless you have a good reason not to.**

 The original documentation  on how to run the connectivity tests can be found on gitlab: [https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/pads.md](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/blob/master/pads.md) 
Check the gitlab documentation for updates, this in a copy (created  22/07/2020) with a bit more explanations for the non-experts in 191. 

* Log in on felix5, skip the ALTI pattern login (because the partition now does it) 
  ```
  ssh -XY nswdaq@pcatlnswfelix05  
  elinkconfig_sTGC_VS
  ```

* Program the pad trigger: load the configuration bit file:
  ```
  source .bashrc 
  program_pad_trigger_fpga 
  ```
  This last command output a command to run, it should be: 
  ```
  fscajtag -d 1 -R 10 -e 2ff /afs/cern.ch/work/n/nswdaq/public/pad_fw/Pad_ro_ilaro_20200610.bit 
  ```
  Run that command

* Start felixcore and OPC server: 
  ```
  felixcore --data-interface eno1 --elinks 737,827 
  ./OpcUaScaServer ~/public/sw/config-ttc/config-files/opc_xml/191/A14/sector_A14_Uncalibration_STG_191_connectivity.xml 
  ```

* **Temperature monitoring**: Once OPC is running start temperature monitoring and keep and eye on it.  Pad trigger temp: ~40C is normal, <60C is safe but not good and >60C ALWAYS POWER OFF PT 
  ```
JSON="/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/A12/padTrigger.json" 
while true; do echo $(date) $(nsw_pad_trigger --config_file $JSON --i2c_reg 1 | grep Readback); sleep 30; done
  ```

* Run partition to take data: 
  ```
  ssh –XY nswdaq@pcatlnswswrod03 
  source /afs/cern.ch/work/n/nswdaq/public/stgc_connectivity/nswdaq/NSWPartitionMaker/191A-ConnectivityPads/setup_part.sh 
  ```
  (! for now? The partition name "ConnectivityTmp"  ) 


* In GUI, subscribe to information 
* To do an sTGC test run:
  ```
  is_write -p part-191A-ConnectivityPads -n Setup.NSW.calibType -t String -v sTGCPadConnectivity -i 
  ```
  `0` Otherwise the default config is for micromegas 

* Run settings:  Check "enable" and the Tier0 project name should be data test
* At `10:49:53 INFORMATION 191A-A14-Calib ers::Message NSWCalibRc::handler::End of handler `
  Stop run 

* For data analysis: 
  ```
  ssh -Y nswdaq@pcatlnswswrod03 
  ```
  Location of file: ls `/tmp/nswdaq/ `


* Known root compatibility issue to make plots, copy file to eos (temp work  around)  [https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/11]( https://gitlab.cern.ch/atlas-muon-nsw-daq/nswstgctriggerconnectivitytest/-/issues/11 )


---

### Latency tests (old):

* Run: ` is_write -p part-191A-ConnectivityPads -n Setup.NSW.calibType -t String -v sTGCPadLatency -i 0`   
  The partition should be shutdown

## Annex: Router to layer correspondence

| **Router** | **sTGC Layer** | Trigger Layers |
| ---------- | -------------- | -------------- |
| R1         | L1             | L0             |
| R2         | L3             | L2             |
| R3         | L5             | L4             |
| R4         | L7             | L6             |
| R5         | L2             | L1             |
| R6         | L4             | L3             |
| R7         | L6             | L5             |
| R8         | L8             | L7             |

