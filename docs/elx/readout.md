# Introduction

## Preface - Machines and users

Depending on the application, you will be logging into different machines, sometimes using multiple sessions. These are:

1. `pcatlnswfelix05` (or just Felix machine): main machine to communicate with the GBTx boards.
2. `pcatlnswswrod03` (or SWROD): machine you will connect to once you are ready to take data.
3. `pccanbustest`: machine generally used to run monitoring software.

You must log to the Felix and SWROD machines using the `nswdaq` user, whose password is `[please ask someone]`.

On the other hand, on pccanbusttest you will log on using `user191`, with the password `user_191`.

All these machines are actual computers on the rack at the back of b191, so if there are any problems, you can go and physically check if everything is on and working.

## Turn on electronics

First of all, turn on the LV on the sector you want to work on:

```
ssh -XY user191@pccanbustest 
FSM_LV_STGC 
```

[Detailed infos are on the LV page](https://stgc-comm-docs.web.cern.ch/LV/lv_dcs/)


## Temperature monitoring

**Always monitor temperature!**

There are 2 ways to monitor temperature:

### 1. MDM_FSM

On the same pccanbustest session, run:

```
ssh -XY user191@pccanbustest 
MDM_FSM
```

This will open the GUI for the T-Sensor monitor. It shows the temperature recorded by the T-Sensors on the sector, as well as the temperatures of the input/output on the cooling pipes.

![](../images/t_sensors_A08.jpeg)

And with that you are ready to take data! Keep an eye on felixcore and the OPC Server, because they might crash during readout, and you'd have to close everything and start again.

### 2. STGSCA

```
ssh -XY user191@pccanbustest
STGSCA
```

This will open the GUI for the SCA temperature monitor. Keep an eye on this while taking data to make sure the boards are being cooled properly.

![](../images/general_setup/sca_temp_monitor.png)

If a board looks red and the temperature isn't updating, don't panic yet: that only means that the connection has been lost. Take a look at https://nsw191.web.cern.ch/nsw191/check.txt to make sure that's the case, and restart the system and try again.



## Data from B180

```
cd /afs/cern.ch/work/s/stgcic/public/sTGC_quick_and_dirty_baselines/trimmers_sTGC
```
