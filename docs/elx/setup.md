# General Setup

## 1. Powering the FEBs

To turn on the LV using the ICS GUI:

```
ssh -XY user191@pccanbustest 
FSM_LV_STGC
```
If it is your first time using the ICS, checkout the full documentation [here](https://stgc-comm-docs.web.cern.ch/LV/lv_dcs/)

More info about the ICS can be found here:
* [Full instruction to run and debug ICS and LV](https://stgc-comm-docs.web.cern.ch/LV/lv_dcs/)
* [ICS how to (obsolete?)](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/LV/ICS%20how%20to.docx?Web=1) 
* [ICS mapping](https://espace.cern.ch/NSWCommissioning/Shared%20Documents/LV/ICS%20channel%20mapping%20dynamic.xlsx?Web=1)

If the HV powering the ICS is not ON already, turn it on ([More details here](https://stgc-comm-docs.web.cern.ch/LV/ngps/))

```
ssh -XY nswdaq@pcatlnswdcs01
NGPS
```




## 2. Start and configure Felix
Two felix computers are now available: felix05 and felix08. Check on the home page of this website which sector is connected to which felix computer. Replace the felix number appropriately in the following comands.

In case of issues checkout the debuging section
```
ssh -XY nswdaq@pcatlnswfelix05
# or: 
ssh -XY nswdaq@pcatlnswfelix08
```
Password: `M***8***!`. You can aslo use `kinit nswdaq` to avoid typing the password all the time.

Just to be sure, performing soft GBT reset, then check that you can see all the links on the felix:
```
flx-init -c 0 
flx-init -c 1 
flx-info GBT -c 0 
flx-info GBT -c 1 
```
You should see:  
**Small sector configuration**  
![image Small sector flx-info result](../images/general_setup/flx-info.png)

**Large sector configuration**  
![image Large sector flx-info result](../images/baseline/LargeSectorflx-info.png)

You can also check the GBT status online here: 
[https://nsw191.web.cern.ch/nsw191/check.txt](https://nsw191.web.cern.ch/nsw191/check.txt)

Check the alignment and compare it to the screenshots. In case of problems, power cycle the low voltage/ICS.

## 3. Configure elinks 

!!! Dead_layers
	Some sectors have dead layers / noisy elinks that need to be removed by hand using the `elinkconfig` GUI. Check the sector specific config page for more details. A08 and A13 have dead layers (05/02/2021)



We now have two configurations possible: Small sector and Large sector. Choose the appropriate options for your run
To configure the elinks run

```
elinkconfig_sTGC_191 <sector option>
```
Where the `<sector option>` can be:

 * Small sector: `HOIP3_SS`
 * Large Sector: `HOIP3_LS`
 * Both sectors running: `HOIP3_SS_LS`

!!! sROC_for_baseline
	sROC elinks are not needed to take baseline ( or trimmers) and they can cause noise. You can disable all their elinks with the elinkconfig GUI to try to solve that issue (05/02/2021)

## 4. Initiatlize GBTx1
Go to the `gbtx_config` directory corresponding to your sector:
```
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/<your favourite sector>
```
Where `<your favourite sector>` is of the format:

 * `191_sTGC_A14`
 * `191_sTGC_A08`
 * etc ... 

Depending on which sector configuration you are using (Large or Small sector), run the appropriate script:

```
./config_gbtx1_SS.py -i # for Small sector configuration
./config_gbtx1_LS.py -i # for Large sector configuration
```
If the right script isn't available it has to be created.
Note that for noise runs and pulser tests you should run without the -i option: `./config_gbtx1.py`
(See pulser test page)

When running for the **first time on a sector**, run a phase training with:

```
./config_gbtx1_SS.py -t # for Small sector configuration
./config_gbtx1_LS.py -t # for Large sector configuration
```



## 5. Start felixcore
```
felixcore_191_sTGC_HOIP_new #felix05
felixcore_191_sTGC_HOIP_new8 #felix08 
```
Keep this terminal open. In case you see strange bugs in the following steps, come back to the feliscore terminal to check it is still running. If felixcore crashed, just restart it.
Only one instance of felixcore can run at the same time. See debug section for more info.

## 6. Initiatlize GBTx2

Now, to configure the second GBTX, since the fibers are not connected, we do it through the first GBTx using a special script.

In a new felix05 terminal, go to the same `gbtx_config` directory as before and initialize GBTx2:
```
ssh -XY nswdaq@pcatlnswfelix05
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/<your favourite sector>
# look for the lastest config, use Felix08 specific files if running on felix08
./config_gbtx2_SS.py -i # for Small sector
./config_gbtx2_LS.py -i # for Large sector
```

If you see the following error, see box below. Don't continue until this is solved  
```
terminate called after throwing an instance of 'Sca::NoReplyException'
  what():  At /afs/cern.ch/user/p/ptzanis/ScaSoftware/Sca/SynchronousService.cpp:165 in std::vector<Sca::Reply> Sca::SynchronousService::SynchronousChannel::sendAndWaitReply(std::vector<Sca::Request>&, unsigned int) reply hasnt come, #requests=1 #received=0
```

** If GBTX2 is very unstable **: Check that ATLI pattern generation is disabled (see DEBUG section in baseline page)  

!!! GBTX2 bug
	Felixcore has trouble configuring too many links one after the other and is prone to crashing, and you might need to redo it 2 or 3 times for it to work, I.e. restarting felixcore, then trying to configure the GBTx2 again. Another possibility is to run two scripts, where we have split this second GBTx into two cards to avoid crashes:
	``` 
	./config_gbtx2_card0<sector type>.py -i
	```   
	```
	./config_gbtx2_card1<sector type>.py -i 
	```
    Where you need to replace `<sector type>` by the sector type (`_LS` or `_SS`) you are using. If this scripts don't exist yet, you can create them


## 7. Start OpcServer
Once the 2 GBTX configured, start the OPCServer on felix05. Go to the directory corresponding to your sector: 
```
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/opc_xml/191/<your sector>
```
Where `<your sector>` is of the format: `A14/` or `A08/`, etc...

List the files in the directory (`ls`) and chose the config xml of the right sector type. For example if A08 is connected as a small sector, you should use: `A08_STG_191_as_Small_Sector_FlxId5.xml`, if it is connected as a large sector, use: `A08_STG_191_as_Large_Sector_FlxId5.xml`

Run the opcServer with (run with the **felix08 version** of the xml if needed!)
```
OpcUaScaServer <small sector xml> # small sector configuration 
OpcUaScaServer --opcua_backend_config /opt/OpcUaScaServer/bin/ServerConfig_LS.xml <large sector xml> # large sector configuration 
```
It should take about a minute to start the OPC Server, but once it starts, it should stay "stable", meaning you wouldn't get any new lines. If it's constantly writing new lines to the terminal, then it's "unstable". Check if felixcore hasn't crashed, and that all the channels are still aligned. If it's unstable, you might have to retry from the beginning, but if not, congratulations! You are almost ready to start taking data. You can rename this terminal "OPC Server", since it will have to run it continually, and any work you do needs to be on a new terminal.

!!! Conclusion
	Congratulation, you are done with the general setup. Please ensure that you started the temperature monitoring. You can now:

	* Take baseline
	* Take pulser data
	* Take a noise run
	* Run the trimmers



## 8. GBTX training

If you see noisy elinks, especially when running pulsers, the GBTX phases must be trained. **FEB must be configured** to run the training!
After following the setup step 1-7 described above, get a partition running (more details on pulsers page):

```
cd ~nswdaq/workspace/public/nswdaq/current/nswdaq/
source setup_nswdaq.sh
source NSWPartitionMaker/setup_pm.sh
cd ../databases/
makeNSWPartition.py 191A-sTGC-Pulser.yml
source  /afs/cern.ch/work/n/nswdaq/public/nswdaq/tdaq-09-02-01/databases/191A-sTGC-Pulser/setup_part.sh
run
```

Configure the FEB using the partition, click on:

- `initialise`
- `config`

Once the partition is done with configuration, you can **kill OPC serve and felixcore**.

You are now ready to run the training on GBTX1:

```
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/<sector>
./config_gbtx1_SS.py -t # for Small sector
./config_gbtx2_SS.py -t # for Small sector
```

GBTX2 training is not needed unless you are running phase 2 test.



## 9. General Debugging

### Starting FELIX after shutdown

If the felix machines have been turned off (e.g power cut), it might be necessary to reprogram the boards and setup the drivers. If FELIX wasn't shutdown, skip this section.

1. Setup the drivers to be able to see the felix cards: 

```
ssh -XY nswdaq@pcatlnswfelix05
sudo /etc/init.d/drivers_flx start 
```

2. Connect the Digilent programmer (USB-to-JTAG) hanging from the FELIX backplane to the FELIX FPGA board that is connected to the motherboard's PCI express slot. On the very end of the FELIX FPGA board there is an associated connector for this  

2. Run this command in the FELIX PC (as a NSW user):  

```
progFELIX
```

3. Move the USB-to-JTAG cable to the second felix card. Run `progFELIX` again.

4. Reboot the computer once the commands above have finished:

```
reboot
```

5. You can ping felix while waiting it to restart:

```
ping pcatlnswfelix05 
```

6. Once felix is reachable again, login and try to run `flx-init` and `flx-info gbt` to check that everything is back to normal.

### Felix CMEM error
```
error: 0x702      => major: Error   2 in package   7 => CMEM RCC library: Failed to open /dev/cmem_rcc
### FlxUpload#0: CMEM_RCC error: 0x702; Failed to allocate requested buffer size
error: 0x703      => major: Error   3 in package   7 => CMEM RCC library: Library has not yet been opened
Configuring Si5345
```
Keep rebooting (`reboot`) felix until that error disappears...
Shutting down and reprogramming do not work

### Check running processes: 

If you get errors, you first need to check if there are no left over processes running in the background: 

```
hey
```

Or the more classic:

```
ps aux | grep Opc  
```

and then kill undesired processes 

```
ps aux | grep felixcore  
pkill -9 -c -f felixcore 
```

If a board seems to give problems regardless of what you try, sometimes reprogramming the felix cards solves the issue. 

### flx-init : LOL register = 0x02

If seeing a lot of:

```
LOL register = 0x02
```

When running `flx-init`, check that the ATLI leds are `ON`. If `OFF` reconfugure ALTI as indicated below.

### felix core Server
```
http://pcatlnswfelix05:8080/
```

### Reconfigure ALTI 

To check if Alti is configured correctly, log on to sbc:   

```
ssh -Y nswdaq@sbcl1ct-191-1  
setupSBC  
menuAltiModule 
2 ["CFG" menu] Global configuration 
3 check ALTI 
```

You should get something like this: 

```
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - CLK mux = "SETUP" 
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - I2C core = "SETUP" 
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - PLL = "LOCKED" 
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - jitter cleaner design = "ALTI_001" (DEFAULT) 
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - ADER1 page register = "SETUP" 
2020-07-09 13:47:15 - INFO in "int LVL1::AltiModule::AltiCheck()": >> ALTI: INFO - TTC encoder = "SETUP" 
```

If not, you can do: 

```
 2 ["CFG" menu] Global configuration 
 1 reset ALTI 
 2 setup ALTI 
 3 check ALTI 
```

Another problem that might happen is that there are no patterns being sent from the ALTI. This can be seen by looking at the front plate of the ALTI;  if the L1A light is ON, this means that patterns are being sent (for example after sTGC trigger link tests). 

This can also be checked without looking at the LEDs in the alti menu by doing: 

```
 9 ["ENC" menu] TTC encoder 
 1 transmitters status 
```

You should get something like this: 

 

The two top ones are the important ones. If they are disabled, you can do: 

```
 9 ["ENC" menu] TTC encoder 
 2 transmitters enable 
 Transmitter (0="TX0", 1="TX1", ..., "10=TX10", 11=all) [0..11]: 0 Enable (0=disable, 1=enable) [0..1]: 1 
 Transmitter (0="TX0", 1="TX1", ..., "10=TX10", 11=all) [0..11]: 1 Enable (0=disable, 1=enable) [0..1]: 1 
```

If everything on Alti looks fine and the events still look empty, make sure the pattern is enabled: in the setup file (setup_oks_NSW-191-TGC-SWROD.sh) one finds the "partition" file the script is using (in this case muons/partitions/part_NSW-191-TGC-SWROD.data.xml). This file in turn, points to various "segments", one of these being the alti segment (vim muons/segments/NSW/NSW-Alti-191-TGC.data.xml)


### Disable ALTI pattern generation  

The Alti pattern generation should be off to take baseline. Otherwise it will create noise that will make OPC, felixcore and/or gbtx2 config crash.

Loging to ATLI and open the config menu, then go to Pattern genertion (`7`) and then status (`1`).

```
ssh -Y nswdaq@sbcl1ct-191-1
setupSBC
menuAltiModule
7  ["PAT" menu] Pattern generation memory
1  status
```

The status should printout something like this:

```
+------------------------------+
| Signal  | Pattern generation |
+------------------------------+
| [0]ORB  | Enabled            |
| [1]L1A  | Enabled            |
| [2]TTR1 | Enabled            |
| [3]TTR2 | Disabled           |
| [4]TTR3 | Enabled            |
| [5]TTYP | Enabled            |
| [6]BGO0 | Enabled            |
| [7]BGO1 | Disabled           |
| [8]BGO2 | Enabled            |
| [9]BGO3 | Enabled            |
+------------------------------+

Pattern generation: Disabled, Repeated

Start address = 0x00000000 (=          0)
Stop  address = 0x00000018 (=         24)


  "status" returns 0
```

The line saying `Pattern generation: Disabled, Repeated` should display `disabled` as shown here. If it shows `enable`, disable it by using `4` in the menu:

```
4  disable   pattern generation
```

Check the status again and retry running.

### Monitor ALTI with netio

You don't record data during the pulser run? To check that ALTI is sending the test pulses, you can spy on the ALTI elink `525115` with netio:

```
netio_cat subscribe -H pcatlnswfelix05 -e raw -p 12350 -t 525115 | tee /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/A14/test.txt 
```



