
## Trimmers/threshold tuning

The different VMM channels have different baseline values and the threshold for signal must be adjusted. To tune the thresholds: 

* Follow the common readout setup  
* Run the threshold tuning: 
```
ssh -Y nswdaq@pcatlnswswrod03 
baselines
# or
# cd /afs/cern.ch/user/n/nswdaq/workspace/public/191_quick_and_dirty_baselines_NEW/stgc-baseline-trimmer-scripts/
source setup.sh 
```
Check the defaults json file used in `stgc_threshold.sh` either modify it or run the script passing the right json as an argument. 
Then run the script:

```
./scripts/stgc_threshold.sh AXX [optional json]
```
Results can be visualized at:

* [http://stgc-trimmer.web.cern.ch/stgc-trimmer/](http://stgc-trimmer.web.cern.ch/stgc-trimmer/)
* If you need the files themselves, they can be found at: `/eos/atlas/atlascerngroupdisk/det-nsw-stgc/trimmers`

## DEBUG
### Missing data / empty plots
If by the end of the run you see something like (median 0) or have unexpected empty plots/rootfiles, this can be due to OPC server crashing.  
Check that it is still running, if not restart OPC and felixcore and restart the trimer run.

```
median 0 rms 11.7676
median 0 rms 0.110222
median 0 rms 23.1763
median 0 rms 19.1049
median 0 rms 18.2753
median 1.77 rms 25.6829
median 0 rms 23.6545
median 3.54 rms 15.9808
```


