# Pulser tests

Follow the general set up to get started.  
**Note that the GBTX configuration is different** than for taking baselines.


## 1. GBTx phases  

The VMM readout is done through the ROC on each FEB. The ROC sends the data to the L1DDC (data driver card) which converts the electrical signals into optical. In order to perform this conversion the L1DDC card has two chip **GBTx1** and **GBTx2**, for our pulser test we receive the data from GBTx1, but still we need to configure the GBTx2 chip in order to keep them silent. Each GBTx1 is one Fiber, therefore one Felix channel in the GBT info command.   

### GBTx training

If the phases have been previously trained, then run with the (-i) option:
```
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/<your favourite sector>
./config_gbtx1_SS.py -i # for Small sector configuration
./config_gbtx1_LS.py -i # for Large sector configuration
```

After starting felixcore, do the same for gbtx2 (which always should use the -i option)

```
ssh -XY nswdaq@pcatlnswfelix05
cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/gbtx_config/<your favourite sector>
./config_gbtx2_SS.py -i # for Small sector
./config_gbtx2_LS.py -i # for Large sector
```


## 2. Preparing  the Data taking

!!! Warning
    **Big update on 06/01/2021**: we are now using partition maker
    (old doc kept for a while in case it is needed but it is otherwise obsolete)

### Create partion with NSWPartitionMaker

If you have to run on a new sector or change the sector you are working with, you must recreate the partition. If not, you can skip this section.

Before recreating the partition, make sure the latest changes to the last json file (eg. VMM recovery) **have been committed on git**

Rongkun's twiki for reference: Twiki: [https://gitlab.cern.ch/atlas-muon-nsw-daq/nswpartitionmaker/-/wikis/191-sTGC-Pulser-Instructions](https://gitlab.cern.ch/atlas-muon-nsw-daq/nswpartitionmaker/-/wikis/191-sTGC-Pulser-Instructions)  

#### 1. Update json files

* Go to:
  ```
  ssh -Y nswdaq@pcatlnswswrod03
  cd /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/NSWPartitionMaker/
  ```
  The json file `STGC_191_AXX_HOIP_TestPulse.json` is the dummy file name used by the partition.

 * If not done by the previous shifter, copy it to the rigth directory and commit it. For example:
  ```
  cp STGC_191_AXX_HOIP_TestPulse.json ../A06/STGC_191_A06_HOIP_TestPulse.json
  git add ../A06/STGC_191_A06_HOIP_TestPulse.json
  git commit -m "sTGC-dev: updated A06 TestPulse json"
  ```
* Now copy the json file of your favourite sector here, for instance:
  ```
	cp ../A02/STGC_191_A02_HOIP_TestPulse_as_Small_Sector.json STGC_191_AXX_HOIP_TestPulse.json
  ```
#### 2. Generate partition 

The partition is generated from a YML file. The YML file has to be updated with the right info before generation:  
```
ssh -Y nswdaq@pcatlnswswrod03
cd ~nswdaq/workspace/public/nswdaq/current/nswdaq/
source setup_nswdaq.sh
source NSWPartitionMaker/setup_pm.sh
cd ../databases/
vim 191A-sTGC-Pulser.yml
```
* Update the `sectors` parameter to your sector. 
* Update the `ttc.patternFile` variable to the appropriate pulser file `cont_altiPat_stgc_pulser.dat`.  (**To be tested!** Old .dat file did not seem to work on 06/01/2021 - alternative file is)

Note that `swROD.useTTC: false` has been set to false temporally (06/01/2021) as it was causing issues.

You are now ready to generate the partition!
```
makeNSWPartition.py 191A-sTGC-Pulser.yml
```

#### 3. Updating a partition

If you need to change your partition's parameters, no need to close the GUI. modify the `.yml` file and run the partition maker with the `-U` option:

```
makeNSWPartition.py - U 191A-sTGC-Pulser.yml
```

On the partition GUI click (top left) on:

```
commit and reload
```



## 3. Data taking

Login to our SWROD machine  
```
ssh -XY nswdaq@pcatlnswswrod03  
```

Then start the Readout-IGUI
```
source  /afs/cern.ch/work/n/nswdaq/public/nswdaq/tdaq-09-02-01/databases/191A-sTGC-Pulser/setup_part.sh
run
```
If you have permission denied errors check the disk usage of /opt/localdisk4/. It may be necessary to delete some files here.

!!! Warning
	When you control the partition exit only with `exit partition`. Do not click on the close button.

If you don't have control, take it in `access control` then click in "run control commands": 
```
Initialize 
```
Some errors might show up in the log. Continue running as long as it works (to be updated) 
```
Config 
```
Remember, the json file that is used to configure the boards has already been set, so if you make any changes, make sure to overwrite them on that same json file. That way we avoid having to close the partition, and then changing the json file in the xml every time. The downside of this is that this file can't contain anything important, since it's constantly being overwritten.

Now, set the number of events you want to record in the `run settings`. Choose 0 for an infinite run. Then start the run using the button: 
```
Start 
```
The run will end automatically when the events are recorded. If you want to stop earlier, press stop. 
Shutdown the IGUI and exit partion: 
```
Unconfig 
Shutdown 
In files: "exit partition" 
```

!!! Debug
"If it's smooth, the partition GUI should open after a few minutes. If not, you can consider run `killit` in the directory, and retry `run`. Then if it still doesn't work, you can consider deleting the `ipc_init_stgc.txt` in the directory. In the worst case, you `reboot` the swrod. " 


With netio: 
 ```
 netio_cat subscribe -H pcatlnswfelix05 -p 12350 -e raw -t 65544 -t 65552 -t 65560 -t 65569 -t 65608 -t 65616 -t 65624 -t 65633 -t 65672 -t 65680 -t 65688 -t  65697 -t 65736 -t 65744 -t 65752 -t 65761 -t 65800 -t 65808 -t 65816 -t 65825 -t 65864 -t 65872 -t 65880 -t 65889 -t 65928 -t 65936 -t 65944 -t 65953 -t 65992 -t 66000 -t 66008 -t 66017 -t 66056 -t 66064 -t 66072 -t 66081 -t 66120 -t 66128 -t 66136 -t 66145 -t 66184 -t 66192 -t 66200 -t 66209 -t 66248 -t 66256 -t 66264 -t 66273  | tee ~/public/sw/config-ttc/config-files/config_json/191/A14/test_gbtx1.txt 
 ```

## 4. Data analysis

Once data has been taken using the GUI or netio, it can be analysed. In a new swrod03 terminal: 
```
ssh -XY nswdaq@pcatlnswswrod03
setup_nsw_process
```
Before running the script, make sure /scripts/quickAnalysis_minitree/hits_RF.py is correct. i.e. the l_board value is set to the sector being read out. Then run the script.
```
./scripts/quickAnalysis_minitree/quick.sh <sector> <pulser> <outputdir> <data>
```
The documentation/code of `quick.sh` is here if needed: [https://gitlab.cern.ch/rowang/nswutilities/-/tree/master/quickAnalysis_minitree](https://gitlab.cern.ch/rowang/nswutilities/-/tree/master/quickAnalysis_minitree)

To look at the plots (we recommend opening a new felix05 terminal), go to: 
```
cd /eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/A12/pulsers/<your_dir>/ 
```
You can then open the folder with your data, and do: 
```
nautilus . 
```
To be able to see the plots more easily. If you want to save the plots to a local directory, open a new terminal and run: 
```
rsync -aPvx nswdaq@lxplus:/eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/A14/pulsers/<your_dir>/data*/  . 
```
This will copy it to the current directory. 

## 5. VMM recovery ( CTRL-phase calibration)

For the debugging we will use the same FELIX GUI as before. 

Start like you are taking pulser data, follow all the steps until the ~Config~ button on the IGUI. 

In another felix terminal, start the felix GUI: 
```
source /afs/cern.ch/work/r/rowang/public/FELIX/setup_nsw_board_ttc.sh  
stgc-dcs 
```

The file that the Readout-IGUI uses to configure the boards is determined in the xml mentioned before. You should be using:
```
/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/AXX/STGC_191_AXX_HOIP_debug.json 
```
We will need to edit this file if we want to see any changes. On FELIX GUI, click on the folder icon, open the json file, and then click  "Load Config". If done correctly, the name of the json file should turn green.   

Then, any changes that are made must be saved by clicking `Write json file` and overwriting the file you are using.
Now, let's say that a VMM doesn't see any hits: 

![](../images/pulser_tests/missing_VMM.png)

(these plots were obtained using the steps from the previous section) 

In this pFEB, the VMM0 doesn't see any of the 1000 pulses we sent. The best way to monitor this is using the "Data Analysis" tab on the  FELIX GUI. To be able to do that, we need to do a run where we store the data sent through that VMM's elink. 

So, to find the elink you need to open the following xml file: 
```
/afs/cern.ch/work/n/nswdaq/public/tdaq-08-03-01/db/NSW_OKS_Test_DB_191/NSW_OKS_Test_DB/muons/segments/NSW/elinks-A14-sTGC-FlxId1.data.xml 
```
And look for the board with the issue, in this case it's pL3Q2. The xml uses the HO/IP convention, so layers 1-4 are IP layers, and layers 5-8 would become HO 1-4. We search "IP_L3Q2pFEB" and we find the corresponding elink: 

![](../images/pulser_tests/FEB_elink.png)

So now, in a felix terminal, we run 
```
netio_cat subscribe -H pcatlnswfelix05 -e raw -p 12350 -t 66072 | tee /afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/A14/test.txt 
```
Where the number after `-t` is the elink, and the number after `-p` is the port. Which port should you use? Port 12350 connects to GBTx1  and has elink range 65536-67583, while port 12351 connects to GBTx2 and has elink range 67584 or higher. So, look at the elink you have, and see which port has it in its range. 

(This command will store all the messages sent through that elink, so that we can later read the hits) 

If we're not taking data, we shouldn't see anything after running the command. Now start a pulser run using the partition (click run on the GUI), and you will see that the command will write the messages from the elink to the test.txt file. Using the FELIX GUI we will be able to see the data more quickly that if we did a run and had to use the analysis script to make the root file and the plots.   

So, after the data taking is done or has been stopped, use CTRL-C to stop the netio command from writing the file. In the FELIX GUI "Data  Analysis" tab, open the test.txt file from the netio command using the folder icon, and the click "Read Data" to load the data in the file.  

Now, clicking `Hits Channel` we will see the same hit plots again.

![](../images/pulser_tests/felix_gui02.png)

Now we will try adjusting the CTRL Phase of the affected VMM. For this, go to the "ROC ASIC" tab, and select the corresponding board from the dropout on the top right (it should say "common").   

Now go to the "ROC Analog Config" tab, and edit the corresponding value under "CTRL Bypass ena" 

NOTE: The ROC uses different names for the VMMs (this unfortunately can't be changed), so take care when modifying the phases: 0 <-> 2,  1 <-> 3,  4 <-> 5,  6 and 7 don't change. 

In our case VMM0 has issues, so we must modify CTRL Phase 2 as per the rule mentioned above. The value was 0, now we changed it to 1. If you continue to have issues, you can try higher numbers. Now, we save our changes to the json file and do "Unconfig" and then "Config" again in the IGUI to upload our changes to the FEBs. 

Before starting the run again, we run the netio command from above to record the data to the file. We run for a few seconds, we stop the  command with CTRL-C, and we take another look with the "Data Analysis" tab: 

![](../images/pulser_tests/felix_gui03.png)

Success! 

## 6. DEBUGGING

### Noisy plots
If you see strange noisy plots like shown below, try retraining GBTX1. See training section on the setup page.
![image](../images/pulser_tests/noisyPulser.png)

### killit
Forgot to exit your partition properly? On it does not start for some reason? Run:
```
killit
```
If still not working remove the following file (located in your partiion folder)
```
rm ipc_init_stgc.text
```
Still no good? try cleanup_part described bellow

### cleanup_part
In case of general issues when generating the partition, you can try to run `cleanup_part`:  

* close your partition
* log onto `nswdaq@sbcl1ct-191-1`
* run `cleanup_part`
* run your partition

### OPC unstable
OPC server crashes because of corrupted data? -> GBTX training.   
See training section on the setup page.

### Partition error messages to be ignored:
List is a work in progress, please add the error messages you encounter but that you can ignore.

1. **BCID mismatch** (18/02/2021):  

  The BCID given by felixcore is shifted with the felix ID but the partition is not setup for that. This should not cause troubes for pulser tests. (but it affects other tests like combined runs?) It will be fixed eventually.
  ```
  15:49:14 ERROR 191A-A12-swROD swrod::OutOfSynchPacketException Data packet #18 from   e-link 0x80019
   with L1ID = 0x12 and BCID = 0x425 is out of synch with L1A data that has L1ID = 0x6b000012 
  and BCID = 0xa3 -- 1000 similar messages suppressed, last occurrence was at 2021-Feb-18 15:49:14,940092 
  ```


---


## Obsolete doc


### Preparing the json file

The json file that the partition will use to configure the boards is determined by this config file:

```
/afs/cern.ch/work/n/nswdaq/public/tdaq-08-03-01/db/NSW_OKS_Test_DB_191/NSW_OKS_Test_DB/muons/segments/NSW/NSW-Config-191-TGC.data.xml
```

If you open this xml using VIM, you will see this:
![](../images/pulser_tests/json_definition_for_partition.png)

If you are running test pulses for a new sector, try to use a json file for an old sector with the phases already callibrated. This should be a good starting  point. You should create dummy file based on that json file, to avoid having to change the xml every time. The recommended name is:  

```
/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/AXX/STGC_191_AXX_HOIP_debug.json 
```

Where XX is the number of the sector you are working on.

### Activating the test pulse registers

We need to make sure the test pulses are activated in the json file. For this, we will use the FELIX GUI. 

You can see the documentation here:  
[https://twiki.cern.ch/twiki/bin/view/Sandbox/Rongkun_FELIXGUI](https://twiki.cern.ch/twiki/bin/view/Sandbox/Rongkun_FELIXGUI)

In a new felix05 terminal run:

```
ssh -XY nswdaq@pcatlnswfelix05
source /afs/cern.ch/work/r/rowang/public/FELIX/setup_nsw_board_ttc.sh  
stgc-dcs 
```

Open the previously defined file with the FELIX GUI by clicking the folder and selecting it, and then click `Load Config` to load it. The name should turn green.

![](../images/pulser_tests/felix_gui01.png)

Now, select the VMM tab and then the `Channel Registers` subtab. The columns `STH` and `ST` should be green. If they aren't, activate them by clicking on `STH`  and `ST`.
Make sure to save your changes by clicking the `Write JSON file` button and selecting the json file you are using.


### Activating the test pulse in Alti

Next, make sure that the pulses are activated on the Alti pattern file. Using a text editor, open the following file:

``` 
/afs/cern.ch/work/n/nswdaq/public/tdaq-08-03-01/db/ttc_patterns/cont_altiPat_stgc.dat
```

You should see this:
Uncomment the highlighted line if it is commented. If it's already uncommented, you're good to go.

![](../images/pulser_tests/alti_pattern.png)


#### Recommended parameters

* **pFEB**:
  VMM tab - Global register 1:

    * Test Pulse DAC [sdp10]: 700
    * Threshold [sdp10]: 400

    VMM tab - Channel Registers:

    * STH : 	OFF
    * ST:	ON

* **sFEB**:
  VMM tab - Global register 1:

    * Test Pulse DAC [sdp10]: 350
    * Threshold [sdp10]: 350

    VMM tab - Channel Registers:

    * STH : 	ON
    * ST:	ON

### Reading the correct elink mapping

The elink mapping read by the partition is set in, for example, /afs/cern.ch/work/n/nswdaq/public/tdaq-08-03-01/db/NSW_OKS_Test_DB_191/NSW_OKS_Test_DB/muons/segments/NSW/NSW-SWROD-191-TGC_LS.data.xml depending on the sector configuration used. This points to the elink xml to be used. 

![](../images/pulser_tests/Screenshot_from_2020-11-17_16-11-31.png)

Set this to the correct elink file. If that file does not exist, it can be created with generate_elinks_oks.py which lives in /afs/cern.ch/user/n/nswdaq/NSWUtilities/nswutilities/app. (Documentation is needed for using this script)

### Subscribing to all elinks through netio

Netio can be used as a debugging tool to check for good size of packets. To subscribe to all packets at once and produce log files for each do the following

```
ssh -YX nswdaq@pcatlnswswrod03
cd /tmp
nswdaq/netiocat-subscriber.sh
```

This produces log files of the format netio-cat-verification-e*.log
Note that this leaves several netio processes running which can cause problems with opc. After running, do
```
pkill -9 -c -f netio_cat
```
