The phase 2 fibers test is the same as the phase 1 pulser test with the following exceptions:

1) The swrod configured in partition need to contain gbtx2 elinks. The PM exists for small sector here
```
https://gitlab.cern.ch/atlas-muon-nsw-daq/nswpartitionmaker/-/blob/master/partitionConfigs/191A-sTGC-Pulser-640.yml. 
```
It is easy to add for large sector.

2) After the sROC is configured (the configure step in the partition) gbtx 2 must be trained
```
./config_gbtx2*640.py -t
```

3) sROC rate and its vmm mapping needs to change to connect to the gbtx2 in board config json files. This can be done on a working phase-1 readout json file by running 
```
/afs/cern.ch/work/r/rowang/public/FELIX/NSWUtilities/app/switch_gbtx_setup.py -i <input.json> -o <output.json> -s sTGC_640
```
