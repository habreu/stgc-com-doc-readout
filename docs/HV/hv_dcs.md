# HV control

To turn on the HV, log in to pccanbustest and launch the HV FSM using the alias below
```
ssh -XY pccanbustest
FSM_HV_STGC
```
Click on `side A` to display the 2 sectors. Next to sector 1 or 2, its state is defined, you can click on it and then go to `go to on` or `go to off` as needed

![image: HV FSM screenshot](../images/HV-LV/HVfsm.png)

# HV tests

For the connectivity test leave HV on for about 15 minutes. For the longterm test it is several days.
To get the data, open:
```
source sTGCgetHVData.sh
```
After selecting the relevant time range, you can find the data sheet in the following folder of the pccanbustest:
```
cd /tmp
```
To plot the data from HV tests, make sure that the following python modules are installed 
```
python -m pip install matplotlib
python -m pip install pandas
```
Then you can run the python scripts below. In each case you have to specify the following arguments:
sector: 1 or 2 (sector number of the data),
sector_number: e.g. 08 (actual sector number),
input_filepath (csv file), 
output_filepath (pdf file)

For the connectivity test run the command (in the right directory) e.g.
```
python connectivity.py 1 08 HV_sectorA08_connectivityTest_08Oct2020.csv connectivityA08.pdf
```
For the longterm test e.g.
```
python longterm.py 1 08 HV_data_A08_12Oct2020-17h00_TO_16Oct2020-12h30.csv longtermA08.pdf
```