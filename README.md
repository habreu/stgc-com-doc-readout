# Documentation repository for NSW sTGC commissionig

All documentation for electronic, services and mappgins will be documented here and published at https://stgc-comm-docs.web.cern.ch

To run inside a docker use:
## Basic Usage - Openshift commands

```sh
$ pip install -r requirements.txt

# if you want to run it locally
$ mkdocs serve

# in order to generate HTML files
$ mkdocs build
```

## For Continuous Deployment

A `.gitlab-ci.yml` file is provided in this repo, which should
automatically deploy the result to AFS every time something is pushed to
the 'master' branch. This uses the
[`ci-web-deployer`](https://gitlab.cern.ch/ci-tools/ci-web-deployer)
developed by our WF colleagues.

In order to specify the exact EOS folder where the compiled files will
be placed, you will have to go to `Settings > CI/CD > Secret variables`.
You should set the following variables:

```
EOS_ACCOUNT_USERNAME = service_account_user
EOS_ACCOUNT_PASSWORD = service_account_password
EOS_MGM_URL = root://eospublic.cern.ch  # or whatever EOS server you use
EOS_PATH = /eos/path/to/your/projects/web/folder
```

`EOS_PATH` must be the same dir that your public-facing EOS site is pointing too.

