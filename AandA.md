# Acronyms and Abbrebiations

**FSM**: Finite State Machine   
**ROC**: Readout Controler   
**sTGC**: small-Strip Thin Gap Chamber   
**ROD**: Readout Driver   
**API**: Application Programming Interface   
**HLT**: High Level Trigger  